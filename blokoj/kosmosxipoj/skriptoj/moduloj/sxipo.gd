extends Node


const base_cabine = preload("res://blokoj/kosmosxipoj/scenoj/moduloj/cabine.tscn")
const base_engine = preload("res://blokoj/kosmosxipoj/scenoj/moduloj/engine.tscn")
const base_cargo = preload("res://blokoj/kosmosxipoj/scenoj/moduloj/cargo.tscn")
const laser = preload("res://blokoj/kosmosxipoj/scenoj/moduloj/laser.tscn")


# функция создания корабля
func create_sxipo(sxipo, objecto):
	var cabine = null
	var cargo = null
	var engine = null
	var moduloj = null
	if (objecto['resurso']['objId'] == 3):# это корабль "Vostok U2" "Базовый космический корабль"
		
		var enganes = false # временно - указывает, чтоодин двигатель уже ставили
		# первый цикл добавляет все модули к кораблю
		var max_konektilo = 0 # максимальный номер комплектующей
#		var tek_konektilo = 1 # текущий номер устанавливаемой в пространстве комплектующей
		# на сколько сдвигать по осям
#		var konektilo_x = 0
#		var konektilo_y = 0
#		var konektilo_z = 0
		for modulo in objecto['ligilo']['edges']:
			if max_konektilo < modulo['node']['konektiloPosedanto']:
				max_konektilo = modulo['node']['konektiloPosedanto']
			if modulo['node']['tipo']['objId'] == 1: # связь типа 1 - модуль корабля
				if modulo['node']['ligilo']['resurso']['objId'] == 4: #"Vostok Модуль Кабины" "Базовый модуль кабины кораблей Vostok"
					cabine = base_cabine.instance()
					cabine.uuid = modulo['node']['ligilo']['uuid']
					# x - в право
					cabine.translation.y = 2 #вверх
					cabine.translation.z = 2 # назад
					cabine.rotate_y(deg2rad(90))
#					cabine.get_node("cabine").translation.y = 2 #вверх
#					cabine.get_node("cabine").translation.z = 2 # назад
#					cabine.get_node("cabine").rotate_y(deg2rad(90))
#					sxipo.get_node("CollisionShape").add_child(cabine)
					sxipo.add_child(cabine)
				elif modulo['node']['ligilo']['resurso']['objId'] == 5: #"Vostok Грузовой Модуль" "Базовый грузовой модуль кораблей Vostok"
					cargo = base_cargo.instance()
					cargo.uuid = modulo['node']['ligilo']['uuid']
					cargo.translation.y = 2
					cargo.translation.z = 8
					cargo.rotate_y(deg2rad(90))
#					cargo.get_node("Cargo").translation.y = 2
#					cargo.get_node("Cargo").translation.z = 8
#					cargo.get_node("Cargo").rotate_y(deg2rad(90))
					sxipo.add_child(cargo)
				elif modulo['node']['ligilo']['resurso']['objId'] == 6: #"Vostok Двигатель" "Базовый двигатель кораблей Vostok"
					engine = base_engine.instance()
					engine.uuid = modulo['node']['ligilo']['uuid']
					if enganes:
						engine.translation.x = -2.5
						engine.translation.y = 1
#						engine.get_node("engine R").translation.z = 10
						engine.translation.z = 11
						engine.rotate_y(deg2rad(270))
						engine.rotate_x(deg2rad(180))
					else:
						engine.translation.x = 2.5
						engine.translation.y = 1
						engine.translation.z = 11
						engine.rotate_y(deg2rad(90))
					sxipo.add_child(engine)
					enganes = !enganes
				elif modulo['node']['ligilo']['resurso']['objId'] == 11: #Универсальный лазер
					moduloj = laser.instance()
					moduloj.uuid = modulo['node']['ligilo']['uuid']
					moduloj.get_node("Turret").translation.y = 3.5
					moduloj.get_node("Turret").translation.z = 1.1
					moduloj.get_node("Platform").translation.y = 3.5
					moduloj.get_node("Platform").translation.z = 1.1
					moduloj.get_node("laser").translation.y = 4.7
					moduloj.get_node("laser").translation.z = 1.1
					
					

					moduloj.get_node("gun_body").translation.y = 4.2
					moduloj.get_node("gun_body").translation.z = 0.6
					
					
#					moduloj.get_node("laser").translation.y = 4.2
#					moduloj.get_node("laser").translation.z = 0.6
					
					
#					moduloj.get_node("laser/gun_stem").translation.y = 4.2
					moduloj.get_node("laser/gun_stem").translation.z = -0.6
#					moduloj.get_node("laser/beam/MeshInstance").translation.y = 4.2
#					moduloj.get_node("laser/beam/MeshInstance2").translation.y = 4.2
#					moduloj.get_node("laser/end_point/Particles").translation.y = 4.2
					sxipo.add_child(moduloj)
