extends HTTPRequest


# Обработчик ответа на HTTP запрос к бэкэнду (ресурсный центр)
# warning-ignore:unused_argument
# warning-ignore:unused_argument
# warning-ignore:unused_argument
func _on_HTTPRequestFind_request_completed(result, response_code, headers, body):
	var resp = body.get_string_from_utf8()
	var parsed_resp = parse_json(resp)
	var simpled_data = parsed_resp['data']['resursoj']['edges']
	
	$'../'.ItemListContent.clear()
	$'../'.get_node("ItemList").clear()
	$'../'.get_node("DetailLabel").set_text("")
	
	for item in simpled_data:
		$'../'.ItemListContent.append(item['node']['nomo']['enhavo'])
	
	$'../'.FillItemList()
