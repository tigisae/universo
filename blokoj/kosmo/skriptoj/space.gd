extends Spatial


const QueryObject = preload("queries.gd")


signal load_objektoj


var id_projekto_direkt_del = [] # список проектов на удаление
var choose = true

var subscription_id


func _ready():
	# подключаем сигнал для обработки входящих данных
	var err = Net.connect("input_data", self, "_on_data")
	if err:
		print('error = ',err)

	Global.fenestro_kosmo = self
	# считываем размер экрана и задаём затемнение на весь экран
	$ui/loading.margin_right = get_node("/root").get_viewport().size.x
	$ui/loading.margin_bottom = get_node("/root").get_viewport().size.y
#	$b_itinero.margin_left = get_node("/root").get_viewport().size.x - 100
#	$b_itinero.margin_top = get_node("/root").get_viewport().size.y-50
	# создаём свой корабль
	var ship = create_ship(Global.direktebla_objekto[Global.realeco-2])
	#если корабль игрока, то брать данные из direktebla_objekto

	$main_camera.translation=Vector3(
		Global.direktebla_objekto[Global.realeco-2]['koordinatoX'],
		Global.direktebla_objekto[Global.realeco-2]['koordinatoY'], 
		Global.direktebla_objekto[Global.realeco-2]['koordinatoZ']+22
	)
	add_child(ship,true)

	for i in get_children():
		if has_signal(i,"new_way_point"):
			i.connect("new_way_point",self,"set_way_point")
	
	subscribtion_kubo()

func _on_data():
	var i_data_server = 0
	for on_data in Net.data_server:
#		print('on_data=', on_data)
#		print('on_data[id] =',on_data['id'],' id_projekto_direkt_del=',id_projekto_direkt_del)
		var index = id_projekto_direkt_del.find(on_data['id'])
#		print('index=',index,' ::: ', typeof(index), " == ", typeof(on_data['id']))
		if index > -1: # находится в спике удаляемых объектов
			print('=== Удалили проект управляемого объекта')
			# удаляем из списка
			var idx_prj = 0 #индекс массива для удаления
			for prj in Global.direktebla_objekto[Global.realeco-2]['projekto']['edges']:
				if prj['node']['uuid']==on_data['payload']['data']['redaktuUniversoProjekto']['universoProjekto']['uuid']:
					Global.direktebla_objekto[Global.realeco-2]['projekto']['edges'].remove(idx_prj)
				idx_prj += 1
			id_projekto_direkt_del.remove(index)
			Net.data_server.remove(i_data_server)
		elif int(on_data['id']) == subscription_id:
			# пришло сообщение, по какому объекту, в каком проекте, какая задача изменена
			sxangxi_tasko(on_data['payload']['data'])
			print('=== subscription_id ===',on_data)
		else:
			print('on_data else=', on_data)
#		elif on_data['payload']['data'].get('filteredUniversoObjekto'):
#			pass
#			Net.data_server.remove(i_data_server)
		i_data_server += 1


# меняем задачу объекту
func sxangxi_tasko(on_data):
	# пометка о нахождении нужного объекта
	var objekto = false
	for ch in get_children(): # проходим по всем созданным объектам в поисках нужного
		if ch.is_in_group('create') and ('objekto' in ch):
			if on_data['universoObjektoEventoj']['objekto']['uuid']==ch.uuid:
				ch.sxangxi_itinero(on_data['universoObjektoEventoj']['projekto'],
					on_data['universoObjektoEventoj']['tasko'])
				objekto = true
	if !objekto: # если объект не найден, то нужно его добавить
		print('найден новый объект!!!')
		pass


# подписка на действие в кубе нахождения
func subscribtion_kubo():
	var q = QueryObject.new()
	subscription_id = Net.current_query_id
	Net.current_query_id += 1
#	Net.send_json(q.test_json(subscription_id))
	Net.send_json(q.kubo_json(subscription_id))


# warning-ignore:unused_argument
func _input(event):
	if Input.is_action_just_pressed("ui_select"):
		choose=!choose
		if choose:
			$main_camera.set_privot($ship)
		else:
			$main_camera.set_privot(null)

func set_way_point(position,dock):
	get_node("ship").add_itinero()
	#останавливаем таймер
	$timer.stop()
	$ship.set_way_point(position,dock)
	$way_point.set_way_point(position)
	#передаём текущие координаты
	$ship.vojkomenco()#начинаем движение
	#запускаем таймер
	$timer.start()

func has_signal(node, sgnl):
	if node == null:
		return false
	node=node.get_signal_list()
	for i in node:
		if i.name == sgnl:
			return true
	return false


# записав проект в базу, получили его uuid
# warning-ignore:unused_argument
# warning-ignore:unused_argument
# warning-ignore:unused_argument
func _on_http_projekto_request_completed(result, response_code, headers, body):
	var resp = body.get_string_from_utf8()
	var parsed_resp = parse_json(resp)
	var simpled_data = parsed_resp['data']['redaktuUniversoProjekto']['universoProjekto']
	var uuid = simpled_data['uuid']
	$"ship".projekto_uuid=uuid
	# теперь создаём задачу с координатами
	var q = QueryObject.new()
	Global.direktebla_objekto[Global.realeco-2]['koordinatoX'] = $ship.translation.x
	Global.direktebla_objekto[Global.realeco-2]['koordinatoY'] = $ship.translation.y
	Global.direktebla_objekto[Global.realeco-2]['koordinatoZ'] = $ship.translation.z
	Global.direktebla_objekto[Global.realeco-2]['rotationX'] = $ship.rotation.x
	Global.direktebla_objekto[Global.realeco-2]['rotationY'] = $ship.rotation.y
	Global.direktebla_objekto[Global.realeco-2]['rotationZ'] = $ship.rotation.z
	$"http_taskoj".request(q.URL, Global.backend_headers, true, 2,
		q.instalo_tasko_posedanto_koord(
			$"ship".uuid, $"ship".projekto_uuid, 
			$"ship".translation.x, #kom_koordX
			$"ship".translation.y, #kom_koordY
			$"ship".translation.z, #kom_koordZ
			Global.itineroj
	))


# warning-ignore:unused_argument
# warning-ignore:unused_argument
# warning-ignore:unused_argument
func _on_http_tasko_request_completed(result, response_code, headers, body):
	var resp = body.get_string_from_utf8()
	var parsed_resp = parse_json(resp)
	var simpled_data = parsed_resp['data']['redaktuUniversoTaskoj']['universoTaskoj']
#	print('===_on_http_tasko_request_completed===',parsed_resp)
	# получаем список задач и помещаем в itinero
	Global.itineroj[0]['uuid_tasko']=simpled_data['uuid']


# warning-ignore:unused_argument
# warning-ignore:unused_argument
# warning-ignore:unused_argument
func _on_http_posedanto_request_completed(result, response_code, headers, body):
	var resp = body.get_string_from_utf8()
# warning-ignore:unused_variable
	var parsed_resp = parse_json(resp)
	pass


# warning-ignore:unused_argument
# warning-ignore:unused_argument
# warning-ignore:unused_argument
func _on_http_finado_request_completed(result, response_code, headers, body):
	var resp = body.get_string_from_utf8()
# warning-ignore:unused_variable
	var parsed_resp = parse_json(resp)
	pass # Replace with function body.


# warning-ignore:unused_argument
# warning-ignore:unused_argument
# warning-ignore:unused_argument
func _on_http_taskoj_request_completed(result, response_code, headers, body):
	var resp = body.get_string_from_utf8()
	var parsed_resp = parse_json(resp)
	var simpled_data = parsed_resp['data']['redaktuKreiUniversoTaskojPosedanto']['universoTaskoj']
	# получаем список задач и помещаем в itinero
	var i = 0
	for tasko in simpled_data:
		if len(Global.itineroj)>i:
			Global.itineroj[i]['uuid_tasko']=tasko['uuid']
		i += 1


const sxipo = preload("res://blokoj/kosmosxipoj/scenoj/sxipo_fremdulo.tscn")
const sxipo_modulo = preload("res://blokoj/kosmosxipoj/skriptoj/moduloj/sxipo.gd")
const base_ship = preload("res://blokoj/kosmosxipoj/scenoj/base_ship.tscn")
const espero = preload("res://blokoj/kosmostacioj/espero/espero_ekster.tscn")
const asteroido = preload("res://blokoj/asteroidoj/ast_1.tscn")

# функция создания корабля
func create_ship(objecto):
	var ship = null
	
	if (objecto['resurso']['objId'] == 3):# это корабль "Vostok U2" "Базовый космический корабль"
		ship = base_ship.instance()
		var sh = sxipo_modulo.new()
		sh.create_sxipo(ship, objecto)
	if not ship: # проверка, если такого корабля нет в программе
		return null
	ship.translation=Vector3(objecto['koordinatoX'],
		objecto['koordinatoY'], objecto['koordinatoZ'])
	if objecto['rotaciaX']:
		ship.rotation=Vector3(objecto['rotaciaX'],
			objecto['rotaciaY'], objecto['rotaciaZ'])
	ship.visible=true
	ship.uuid=objecto['uuid']
	ship.add_to_group('create')
	$main_camera.set_privot(ship)
	return ship


func _on_space_load_objektoj():
	Title.get_node("CanvasLayer/UI/Objektoj/Window").distance_to($ship.translation)

	# и теперь по uuid нужно найти проект и задачу
	var projektoj = Global.direktebla_objekto[Global.realeco-2]['projekto']['edges']
	var q = QueryObject.new()
	if len(projektoj)>1:
		print('нужно обнулить все проекты!!! должно быть на стороне сервера')
		#нужно обнулить все проекты!!! должно быть на стороне сервера
		for prj in projektoj:
			var id = Net.current_query_id
			Net.current_query_id += 1
			id_projekto_direkt_del.push_back(id)
			Net.send_json(q.finado_projeko_json(prj['node']['uuid'],id))
		projektoj.clear()
	if len(projektoj)>0:
		if len(projektoj[0]['node']['tasko']['edges'])==0:
			print('нужно закрыть проект!!!')
			id_projekto_direkt_del.push_back(Net.current_query_id)
			Net.current_query_id += 1
			Net.send_json(q.finado_projeko_json(projektoj[0]['node']['uuid'],id_projekto_direkt_del[len(id_projekto_direkt_del)-1]))
			pass# нужно закрыть проект!!!
		else:
			$ship.projekto_uuid=projektoj[0]['node']['uuid']
			#заполняем маршрут
			Global.itineroj.clear()
			for tasko in projektoj[0]['node']['tasko']['edges']:
				if tasko['node']['statuso']['objId']==2:#задачу, которая "В работе" ставим первой
					Global.fenestro_itinero.add_itinero(
						tasko['node']['uuid'],
						'',
						'координаты в космосе',
						tasko['node']['finKoordinatoX'],
						tasko['node']['finKoordinatoY'],
						tasko['node']['finKoordinatoZ'],
						$ship.translation.distance_to(Vector3(
							tasko['node']['finKoordinatoX'],
							tasko['node']['finKoordinatoY'],
							tasko['node']['finKoordinatoZ']
					)))
					break;
			for tasko in projektoj[0]['node']['tasko']['edges']:
				if tasko['node']['statuso']['objId']==1:# добавляем остальные задачи
					Global.fenestro_itinero.add_itinero(
						tasko['node']['uuid'],
						'',
						'координаты в космосе',
						tasko['node']['finKoordinatoX'],
						tasko['node']['finKoordinatoY'],
						tasko['node']['finKoordinatoZ'],
						$ship.translation.distance_to(Vector3(
							tasko['node']['finKoordinatoX'],
							tasko['node']['finKoordinatoY'],
							tasko['node']['finKoordinatoZ']
					)))
			if len(Global.itineroj)==0:
				$ship.projekto_uuid='' #задач на полёт нет, проект надо бы закрыть
			else:
				#отправляем корабль по координатам
				var position = Vector3(Global.itineroj[0]['koordinatoX'],
					Global.itineroj[0]['koordinatoY'],
					Global.itineroj[0]['koordinatoZ'])
				Global.fenestro_itinero.FillItemList()
				$ship.set_way_point(position,null)
				$"way_point".set_way_point(position)
				$timer.start()
				Global.fenestro_itinero.get_node("canvas/MarginContainer/VBoxContainer/HBoxContainer/kom_itinero").disabled=true
				Global.fenestro_itinero.get_node("canvas/MarginContainer/VBoxContainer/HBoxContainer/itinero_next").disabled=false
				Global.fenestro_itinero.get_node("canvas/MarginContainer/VBoxContainer/HBoxContainer/itinero_fin").disabled=false
				Global.fenestro_itinero.get_node("canvas/MarginContainer/VBoxContainer/HBoxContainer/itinero_clear").disabled=false
	else:
		$ship.projekto_uuid=''

	# создаём остальные объекты в космосе
	for item in Global.objektoj:
		if item['resurso']['objId'] == 1:#объект станция Espero
			var state = espero.instance()
			state.translation=Vector3(item['koordinatoX'],
				item['koordinatoY'], item['koordinatoZ'])
			state.uuid = item['uuid']
			state.objekto = item.duplicate(true)
			add_child(state)
			state.add_to_group('state')
			state.add_to_group('create')
		elif (item['resurso']['tipo']['objId'] == 2)and(item['koordinatoX']):# тип - корабль
			var s = sxipo.instance()
			var sh = sxipo_modulo.new()
			sh.create_sxipo(s, item)
			s.uuid = item['uuid']
			s.objekto = item.duplicate(true)
			s.translation=Vector3(item['koordinatoX'],
				item['koordinatoY'], item['koordinatoZ'])
			s.rotation=Vector3(item['rotaciaX'],
				item['rotaciaY'], item['rotaciaZ'])
			# добавляем маршрут движения
			projektoj = item['projekto']['edges']
			if len(projektoj)==1:
				s.projekto_itineroj=projektoj[0]['node']['uuid']
				#заполняем маршрут
				for tasko in projektoj[0]['node']['tasko']['edges']:
					if tasko['node']['statuso']['objId']==2:#задачу, которая "В работе" ставим первой
#						cam.point_of_interest.route.push_back(Transform(Basis.IDENTITY, cam.project_position(get_viewport().get_mouse_position(),z_away)))#Если кнопка нажата, то бросаем луч из камеры на глубину z_away и получаем точку, тут же устанавливаем новый вейпойнт с координатами
						s.itineroj.push_front({
							'uuid':tasko['node']['uuid'],
							'koordinatoX':tasko['node']['finKoordinatoX'],
							'koordinatoY':tasko['node']['finKoordinatoY'],
							'koordinatoZ':tasko['node']['finKoordinatoZ'],
							'pozicio': tasko['node']['pozicio']
						})
						break;
				for tasko in projektoj[0]['node']['tasko']['edges']:
					if tasko['node']['statuso']['objId']==1:# добавляем остальные задачи
						s.itineroj.push_back({
							'uuid':tasko['node']['uuid'],
							'koordinatoX':tasko['node']['finKoordinatoX'],
							'koordinatoY':tasko['node']['finKoordinatoY'],
							'koordinatoZ':tasko['node']['finKoordinatoZ'],
							'pozicio': tasko['node']['pozicio']
						})
				s.set_route(s.itineroj.duplicate())
			add_child(s)
			s.add_to_group('create')
			s.add_to_group('enemies')
		elif (item['resurso']['objId'] == 7)and(item['koordinatoX']):# тип - астероид
			var ast = asteroido.instance()
			ast.translation=Vector3(item['koordinatoX'],
				item['koordinatoY'], item['koordinatoZ'])
			ast.rotation=Vector3(item['rotaciaX'],
				item['rotaciaY'], item['rotaciaZ'])
			ast.uuid = item['uuid']
			ast.objekto = item.duplicate(true)
			add_child(ast)
			ast.add_to_group('create')
			ast.add_to_group('asteroidoj')


func _on_Timer_timeout():
	var q = QueryObject.new()
	# Делаем запрос к бэкэнду
	Global.direktebla_objekto[Global.realeco-2]['koordinatoX'] = $ship.translation.x
	Global.direktebla_objekto[Global.realeco-2]['koordinatoY'] = $ship.translation.y
	Global.direktebla_objekto[Global.realeco-2]['koordinatoZ'] = $ship.translation.z
	Global.direktebla_objekto[Global.realeco-2]['rotationX'] = $ship.rotation.x
	Global.direktebla_objekto[Global.realeco-2]['rotationY'] = $ship.rotation.y
	Global.direktebla_objekto[Global.realeco-2]['rotationZ'] = $ship.rotation.z
	$http_mutate.request(q.URL, Global.backend_headers, true, 2, 
		q.objecto_mutation($ship.uuid, $ship.translation.x, 
			$ship.translation.y, $ship.translation.z,
			$ship.rotation.x, 
			$ship.rotation.y, $ship.rotation.z)
	)



func _on_space_tree_exiting():
	#разрушаем все созданные объекты в этом мире
	for ch in get_children():
		if ch.is_in_group('create'):
			ch.free()
