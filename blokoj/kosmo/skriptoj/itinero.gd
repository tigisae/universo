extends Control


func _ready():
	Global.fenestro_itinero = self


func _on_Close_button_pressed():
	$"canvas/MarginContainer".set_visible(false)


func _resize(event: InputEvent) -> void:
	if event is InputEventMouseMotion and Input.is_mouse_button_pressed(BUTTON_LEFT):
		$canvas/MarginContainer.rect_size += event.relative


func _drag(event: InputEvent) -> void:
	if event is InputEventMouseMotion and Input.is_mouse_button_pressed(BUTTON_LEFT):
		$canvas/MarginContainer.rect_position += event.relative


func FillItemList():
	$"canvas/MarginContainer/VBoxContainer/ItemList".clear()
	# Заполняет список найдеными продуктами
	for Item in Global.itineroj:
		get_node("canvas/MarginContainer/VBoxContainer/ItemList").add_item('('+String(int(Item['distance']))+') '+Item['nomo'], null, true)

#пересчет дистанции до объектов в списке
func distance_to(trans):
	for obj in Global.itineroj:
		obj['distance'] = trans.distance_to(Vector3(obj['koordinatoX'],
			obj['koordinatoY'],obj['koordinatoZ']))
	$'canvas/MarginContainer/VBoxContainer/ItemList'.clear()
	FillItemList()


#отправляем корабль в полёт
func go_ship():
	if len(Global.itineroj)==0:
		return 404
	if Global.fenestro_kosmo:
		var position = Vector3(Global.itineroj[0]['koordinatoX'],Global.itineroj[0]['koordinatoY'],Global.itineroj[0]['koordinatoZ'])
		Global.fenestro_kosmo.get_node("ship").set_way_point(position,null)
		Global.fenestro_kosmo.get_node("way_point").set_way_point(position)

func komenci_itinero():
	if Global.fenestro_kosmo:
		Global.fenestro_kosmo.get_node("ship").vojkomenco()#начинаем движение
		# отправляем в полёт
		go_ship()
		#запускаем таймер
		Global.fenestro_kosmo.get_node("timer").start()
		$canvas/MarginContainer/VBoxContainer/HBoxContainer/kom_itinero.disabled=true


func _on_kom_itinero_pressed():
	if Global.fenestro_kosmo:
		#остановить текущие задачи
		# останавливаем таймер передачи данных на сервер
		Global.fenestro_kosmo.get_node("timer").stop()
		#отправка последних координат и закрытие задачи с проектом
		Global.fenestro_kosmo.get_node("ship").finofara_flugo()
		#создать проект
		#создать список задач на основе списка itineroj
		komenci_itinero()


func add_itinero(uuid_tasko, uuid, nomo, koordX, koordY, koordZ, distance):
	Global.itineroj.append({
		'uuid_tasko':uuid_tasko,
		'uuid':uuid,
		'nomo':nomo,
		'koordinatoX':koordX,
		'koordinatoY':koordY,
		'koordinatoZ':koordZ,
		'distance':distance,
	})
	FillItemList()

func clear_itinero():
	Global.itineroj.clear()
	FillItemList()

func _on_itinero_fin_pressed():
	if Global.fenestro_kosmo:
		#если в полёте
		if $canvas/MarginContainer/VBoxContainer/HBoxContainer/kom_itinero.disabled:
			if $canvas/MarginContainer/VBoxContainer/HBoxContainer/itinero_fin.text=='Пауза':
				#останавливаем движение корабля
				$canvas/MarginContainer/VBoxContainer/HBoxContainer/itinero_fin.text='Далее'
				$canvas/MarginContainer/VBoxContainer/HBoxContainer/itinero_next.disabled=true
				Global.fenestro_kosmo.get_node("ship").clear_way_point()
				Global.fenestro_kosmo.get_node("way_point").set_active(false)
			else:
				$canvas/MarginContainer/VBoxContainer/HBoxContainer/itinero_fin.text='Пауза'
				#продолжаем движение корабля
				$canvas/MarginContainer/VBoxContainer/HBoxContainer/itinero_next.disabled=false
				go_ship()
				# itinero_clear

func _on_itinero_clear_pressed():
	if len(Global.itineroj)==0:
		return 404
	if Global.fenestro_kosmo:
		#закрыть все задачи и проект
		#удаляем все задачи далее первой
		Global.itineroj = [Global.itineroj[0],]
		#закрываем текущую задачу и проект автоматически закрывается
		Global.fenestro_kosmo.get_node("ship").clear_way_point()
		Global.fenestro_kosmo.get_node("way_point").set_active(false)
		Global.fenestro_kosmo.get_node("timer").stop()
		Global.fenestro_kosmo.get_node("ship").finofara_flugo()
		$canvas/MarginContainer/VBoxContainer/HBoxContainer/kom_itinero.disabled=false
		$canvas/MarginContainer/VBoxContainer/HBoxContainer/itinero_next.disabled=true
		$canvas/MarginContainer/VBoxContainer/HBoxContainer/itinero_fin.disabled=true
		$canvas/MarginContainer/VBoxContainer/HBoxContainer/itinero_clear.disabled=true


# пропустить текущую цель и лететь к следующей
func _on_itinero_next_pressed():
	if Global.fenestro_kosmo:
		Global.fenestro_kosmo.get_node("ship").clear_way_point()
		Global.fenestro_kosmo.get_node("way_point").set_active(false)
		Global.fenestro_kosmo.get_node("timer").stop()
		Global.fenestro_kosmo.get_node("ship").finofara_flugo()
