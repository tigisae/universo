extends "res://kerno/fenestroj/tipo1.gd"

const QueryObject = preload("queries.gd")

var ItemListContent = []

func _ready():
#	Title.connect("load_taskoj", self, "_reload_taskoj")
	pass
	

# перезагружаем список объектов
func _reload_taskoj():
	$"canvas/MarginContainer/VBoxContainer/scroll/ItemList".clear()
	FillItemList()
	
func FillItemList():
	# Заполняет список найдеными продуктами
	for Item in ItemListContent:
		get_node("canvas/MarginContainer/VBoxContainer/scroll/ItemList").add_item(Item, null, true)




# warning-ignore:unused_argument
func _on_ItemList_item_selected(index):
	var q = QueryObject.new()
	$HTTPTaskojRequestFind.request(q.URL, Global.backend_headers, true, 2, q.taskoj_query())
	$canvas/MarginContainer/VBoxContainer/Label.text = "Задачи"
	
# Вызывается перед появлением окна

func _on_Window_draw():
	var q = QueryObject.new()

	# Делаем запрос к бэкэнду
	$HTTPProjektoRequestFind.request(q.URL, Global.backend_headers, true, 2, q.taskoj_projekto())
