extends HTTPRequest


# Обработчик ответа на HTTP запрос к бэкэнду

# warning-ignore:unused_argument
# warning-ignore:unused_argument
# warning-ignore:unused_argument
func _on_HTTPProjektoRequestFind_request_completed(result, response_code, headers, body):
	var resp = body.get_string_from_utf8()
	var parsed_resp = parse_json(resp)
	var simpled_data = parsed_resp['data']['universoProjekto']['edges']
	
	$'../'.ItemListContent.clear()
	$'../canvas/MarginContainer/VBoxContainer/scroll/ItemList'.clear()
	# $'../MarginContainer/VBoxContainer/'.get_node("DetailLabel").set_text("")
	
	for item in simpled_data:
		$'../'.ItemListContent.append(item['node']['nomo']['enhavo'])
	
	$'../'.FillItemList()
