extends "res://kerno/fenestroj/tipo1.gd"


onready var _button = $canvas/MarginContainer/VBoxContainer/kosmo
onready var _label = $canvas/MarginContainer/VBoxContainer/Label


const QueryObject = preload("res://kerno/menuo/skriptoj/queries.gd")


# ищем в списке объектов объект с конкретным uuid
func search_objekt_uuid(uuid):
	for objekt in Global.objektoj:
		if objekt['uuid']==uuid:
			return objekt['nomo']['enhavo']
	return 'не найден'


func print_button():
	if Global.direktebla_objekto[Global.realeco-2]['kosmo']:
		_label.text = 'Центр взаимодействия'
		_button.text='Войти в станцию'
		_button.disabled=true
		_button.set_visible(false)
	else:
		if Global.direktebla_objekto[Global.realeco-2].get('ligiloLigilo'):
			if len(Global.direktebla_objekto[Global.realeco-2]['ligiloLigilo']['edges'])>0:
				var uuid = Global.direktebla_objekto[Global.realeco-2]['ligiloLigilo']['edges'][0]['node']['uuid']
			
				_label.text = search_objekt_uuid(uuid)# Global.direktebla_objekto[Global.realeco-2]
				_button.text='Выйти в космос'
				_button.disabled=false
				_button.set_visible(true)


func _on_kosmo_pressed():
	if Global.direktebla_objekto[Global.realeco-2]['kosmo']:
		go_kosmostacioj()
	else:
		go_kosmo()
	$canvas/MarginContainer.set_visible(false)

# вход в ближайшую станцию (задача - вычислить её в списке объектов)
func go_kosmostacioj():
		# Разрегистрируем обработчик сигнала request_completed (вызывается
	# по завершении HTTPRequest)
#	Title.get_node("request").connect('request_completed', Title, '_on_eniri_kosmostacio_request_completed')
#	var q = QueryObject.new()
#	# закрываем проект
#	#  добавляем запись в связи, что находимся внутри
#	var uuid_tasko = ''
#	if $"../ship".projekto_uuid:
#		uuid_tasko = Global.itineroj[0]['uuid_tasko']
#	var error = Title.get_node("request").request(q.URL_DATA, 
#		Global.backend_headers,
#		true, 2, q.eniri_kosmostacio(
#			$"../ship".projekto_uuid,
#			uuid_tasko, 
#			Global.objektoj[index_pos]['uuid']))
#	# Если запрос не выполнен из-за какой-то ошибки
#	# TODO: Такие ошибки наверное нужно как-то выводить пользователю?
#	if error != OK:
#		print('Error in GET (_on_eniri_kosmostacio_request_completed) Request.')
#	# добавляем в данные пользователя о станции для последующего выхода
#	if !Global.direktebla_objekto[Global.realeco-2].get('ligiloLigilo'):
#		Global.direktebla_objekto[Global.realeco-2]['ligiloLigilo'] = {'edges':[]}
#	Global.direktebla_objekto[Global.realeco-2]['ligiloLigilo']['edges'].append({
#		'node': {'posedanto': {'koordinatoX' : Global.objektoj[index_pos]['koordinatoX'],
#		'koordinatoY' : Global.objektoj[index_pos]['koordinatoY'],
#		'koordinatoZ' : Global.objektoj[index_pos]['koordinatoZ'],
#		'kubo': {'objId' : Global.kubo},},
#		'uuid' : Global.objektoj[index_pos]['uuid']},})
#	Title.CloseWindow()
#	Global.direktebla_objekto[Global.realeco-2]['kosmo'] = false
#	# вызываем сцену станции
#	get_tree().change_scene('res://blokoj/kosmostacioj/CapKosmostacio.tscn')
	pass

func go_kosmo():
	if not Global.direktebla_objekto[Global.realeco-2].has('uuid'):
		print('Нет корабля для этого мира')
		return
	var q = QueryObject.new()
	# Делаем запрос к бэкэнду для получения списка управляемых объектов.
	# Ответ будет обрабатываться в функции get_direktebla_request_complete
	var del_uuid = '' #есть ли в базе запись о нахождении в станции
	# задаём координаты выхода из станции, согласно координатам станции
	if len(Global.direktebla_objekto[Global.realeco-2]['ligiloLigilo']['edges'])>0:
		Global.direktebla_objekto[Global.realeco-2]['koordinatoX'] = \
			Global.direktebla_objekto[Global.realeco-2]['ligiloLigilo']['edges'][0]['node']['posedanto']['koordinatoX'] + 120
		Global.direktebla_objekto[Global.realeco-2]['koordinatoY'] = \
			Global.direktebla_objekto[Global.realeco-2]['ligiloLigilo']['edges'][0]['node']['posedanto']['koordinatoY'] + 120
		Global.direktebla_objekto[Global.realeco-2]['koordinatoZ'] = \
			Global.direktebla_objekto[Global.realeco-2]['ligiloLigilo']['edges'][0]['node']['posedanto']['koordinatoZ'] + 200
		del_uuid = Global.direktebla_objekto[Global.realeco-2]['ligiloLigilo']['edges'][0]['node']['uuid']
		Global.kubo = Global.direktebla_objekto[Global.realeco-2]['ligiloLigilo']['edges'][0]['node']['posedanto']['kubo']['objId']
		Global.direktebla_objekto[Global.realeco-2]['rotaciaX'] = 0
		Global.direktebla_objekto[Global.realeco-2]['rotaciaY'] = 0
		Global.direktebla_objekto[Global.realeco-2]['rotaciaZ'] = 0
		Global.direktebla_objekto[Global.realeco-2]['kosmo'] = true
		#удаляем в массиве объектов пользователя указатель на станцию
		Global.direktebla_objekto[Global.realeco-2]['ligiloLigilo']['edges'].clear()
	# Разрегистрируем обработчик сигнала request_completed (вызывается
	# по завершении HTTPRequest)
# warning-ignore:return_value_discarded
	Title.get_node("request").connect('request_completed', Title, 'komenci_request_complete')
	var error = Title.get_node("request").request(q.URL_DATA, 
		Global.backend_headers, true, 2, 
		q.go_objekt_kosmo_query(
			Global.direktebla_objekto[Global.realeco-2]['uuid'],
			Global.direktebla_objekto[Global.realeco-2]['koordinatoX'],
			Global.direktebla_objekto[Global.realeco-2]['koordinatoY'], 
			Global.direktebla_objekto[Global.realeco-2]['koordinatoZ'],
			0,0,0, del_uuid, Global.kubo
		))
#	go_objekt_kosmo_query(uuid, koordX, koordY, koordZ, rotaciaX, rotaciaY, rotaciaZ, uuid_ligilo_del, kuboId = 1):	
# warning-ignore:return_value_discarded
	get_tree().change_scene('res://blokoj/kosmo/scenoj/space.tscn')

	# Если запрос не выполнен из-за какой-то ошибки
	# TODO: Такие ошибки наверное нужно как-то выводить пользователю?
	if error != OK:
		print('Error in GET (direktebla) Request.')


