extends Node


# Залогинен или нет
var status = false
# id пользователя в бэкэнде
var id
# логин в бэкэнде
var login = ''
# ник в бэкэнде
var nickname = ''
var nickname_uuid = ''
# в какой реальности сейчас находится
var realeco = 1 # при запуске находимся в реальном мире
# в каком кубе сейчас находимся
var kubo = 1
# Заголовки передаваемые с запросом к бэкэнду
var backend_headers = ["Content-Type: application/json"]
# Массив объектов, который мы наблюдаем в текущий момент
var objektoj = []
# признак окончания загрузки объектов. Пока false - кнопки перехода в параллельные миры не работают
var loading = false
# Управляемые объекты и их нахождение - в космосе или на станции
#  (на 01.05.2020 это обязательно корабли)
# Параллельные миры идут по порядку в соответсвии с базой минус один (реальная),
#  начиная с 0
var direktebla_objekto = [{'kosmo':false,},{'kosmo':false,},]
# маршрут движения управляемого объекта (список проектов с задачей маршрута)
var itineroj = [] 
#uuid_tasko - uuid задачи, когда уже летим
#uuid - uuid цели полёта, если это объект 
#nomo - название объекта цели 
# координаты цели полёта
#			'koordinatoX':
#			'koordinatoY':
#			'koordinatoZ':
# расстояние до цели полёта - distance
# окно (сцена) работы с маршрутом
var fenestro_itinero
# сцена текущего космоса
var fenestro_kosmo

func scene(name):
# warning-ignore:return_value_discarded
	get_tree().change_scene('res://blokoj/rajtigo/'+name+'.tscn')
	
	# =Global.direktebla_objekto[objekt[node][realeco[objId]]=={koordinatoX:49905.585938, koordinatoY:512.045044, koordinatoZ:5.943917, ligilo:{edges:[{node:{ligilo:{ligilo:{edges:[...]}, nomo:{enhavo:Vostok Двигатель}}, tipo:{objId:1}}}, {node:{ligilo:{ligilo:{edges:[...]}, nomo:{enhavo:Vostok Двигатель}}, tipo:{objId:1}}}, {node:{ligilo:{ligilo:{edges:[{node:{konektiloLigilo:4, konektiloPosedanto:3, ligilo:{uuid:047b37d5-eb15-4548-8eb8-2de6913dd6a3}}}, {node:{konektiloLigilo:3, konektiloPosedanto:4, ligilo:{uuid:a9f5962d-7b7b-4007-a8ae-c07d7a8c2e18}}}]}, nomo:{enhavo:Vostok Грузовой Модуль}}, tipo:{objId:1}}}, {node:{ligilo:{ligilo:{edges:[{node:{konektiloLigilo:Null, konektiloPosedanto:Null, ligilo:{uuid:bc40a651-7415-4b5d-9e89-1f2a9d22cac5}}}, {node:{konektiloLigilo:1, konektiloPosedanto:2, ligilo:{uuid:08d6d319-5d5f-462e-b1be-7532c881ed9c}}}]}, nomo:{enhavo:Vostok Модуль Кабины}}, tipo:{objId:1}}}]}, ligiloLigilo:{edges:[...]}, nomo:{enhavo:Vostok Vasiliy}, posedanto:{edges:[{node:{posedantoUzanto:{siriusoUzanto:{objId:6417}}}}]}, posedantoObjekto:Null, priskribo:{enhavo:Базовый космический корабль}, projekto:{edges:[]}, realeco:{objId:2}, resurso:{nomo:{enhavo:Vostok U2}, objId:3, priskribo:{enhavo:Базовый космический корабль}, tipo:{nomo:{enhavo:Космический корабль}, objId:2}}, rotaciaX:0.631496, rotaciaY:1.489518, rotaciaZ:0, uuid:1790592a-8f10-4ca1-a829-9dced5975504
func _ready():
	OS.window_maximized = true
